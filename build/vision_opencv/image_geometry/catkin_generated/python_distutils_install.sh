#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/vision_opencv/image_geometry"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/install/lib/python2.7/dist-packages:/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build" \
    "/usr/bin/python" \
    "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/vision_opencv/image_geometry/setup.py" \
    build --build-base "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build/vision_opencv/image_geometry" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/install" --install-scripts="/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/install/bin"
