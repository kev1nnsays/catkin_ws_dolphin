# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  )
# The set of files for implicit dependencies of each language:

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"opencv_apps\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/devel/include"
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/vision_opencv/opencv_apps/include"
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/vision_opencv/cv_bridge/include"
  "/opt/ros/jade/include"
  "/usr/local/include/opencv"
  "/usr/local/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
