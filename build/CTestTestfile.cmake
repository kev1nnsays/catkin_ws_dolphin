# CMake generated Testfile for 
# Source directory: /home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src
# Build directory: /home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(vision_opencv/opencv_tests)
SUBDIRS(prosilica_gige_sdk)
SUBDIRS(vision_opencv/image_geometry)
SUBDIRS(vision_opencv/vision_opencv)
SUBDIRS(vision_opencv/cv_bridge)
SUBDIRS(vision_opencv/opencv_apps)
SUBDIRS(prosilica_driver/prosilica_camera)
