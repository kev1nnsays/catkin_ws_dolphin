# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/camera1394/src/nodes/camera1394_node.cpp" "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build/camera1394/src/nodes/CMakeFiles/camera1394_node.dir/camera1394_node.cpp.o"
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/camera1394/src/nodes/dev_camera1394.cpp" "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build/camera1394/src/nodes/CMakeFiles/camera1394_node.dir/dev_camera1394.cpp.o"
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/camera1394/src/nodes/driver1394.cpp" "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build/camera1394/src/nodes/CMakeFiles/camera1394_node.dir/driver1394.cpp.o"
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/camera1394/src/nodes/features.cpp" "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build/camera1394/src/nodes/CMakeFiles/camera1394_node.dir/features.cpp.o"
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/camera1394/src/nodes/format7.cpp" "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build/camera1394/src/nodes/CMakeFiles/camera1394_node.dir/format7.cpp.o"
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/camera1394/src/nodes/modes.cpp" "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build/camera1394/src/nodes/CMakeFiles/camera1394_node.dir/modes.cpp.o"
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/camera1394/src/nodes/registers.cpp" "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build/camera1394/src/nodes/CMakeFiles/camera1394_node.dir/registers.cpp.o"
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/src/camera1394/src/nodes/trigger.cpp" "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/build/camera1394/src/nodes/CMakeFiles/camera1394_node.dir/trigger.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"camera1394\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/kev1nnsays/Documents/DolphinProject/catkin_ws_dolphin/devel/include"
  "/opt/ros/jade/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
