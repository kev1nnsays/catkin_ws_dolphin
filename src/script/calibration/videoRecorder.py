#!/usr/bin/env python

from __future__ import print_function
import roslib
#roslib.load_manifest('../src/prosilica_driver/prosilica_camera/package.xml')
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


class image_converter:

  def __init__(self, out):

    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("image_raw",Image,self.callback, out)

  def callback(self,data, out):

    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      out.write(cv_image)
    except CvBridgeError as e:
      print(e)

    cv2.imshow("Image window", cv_image)

    if cv2.waitKey(3) & 0xFF == 27:
      cv2.destroyAllWindows()
      out.release()
      print ("Video out released")


def main(args):
  fourcc = cv2.VideoWriter_fourcc(*'XVID')
  out = cv2.VideoWriter('output.avi',fourcc, 20.0, (1360,1024))
  print ("writing") 
  ic = image_converter(out)
  rospy.init_node('image_converter', anonymous=True)
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)

