import cv2
import numpy as np

source = cv2.VideoCapture('calibrationVideo.mp4')
calData = np.load('calibration_data.npz')
d = calData['distCoeff.npy']
K = calData['intrinsic_matrix.npy']
cv2.namedWindow('Undistorted')
while(1):
    ret, frame = source.read()
    h,w = frame.shape[:2]
    newcamera, roi = cv2.getOptimalNewCameraMatrix(K,d,(w,h),alpha=1,centerPrincipalPoint=0)
    undistortedframe = cv2.undistort(frame,K,d,None,newcamera)
    comb = np.hstack((frame, undistortedframe))
    comb = cv2.resize(comb, None, fx=0.25, fy=0.25, interpolation=cv2.INTER_CUBIC)
    cv2.imshow('Undistorted', comb)
    key = cv2.waitKey(10) & 0xff
    if key == 27:
        cv2.destroyAllWindows()
        source.release()
        sys.exit("Exit")
