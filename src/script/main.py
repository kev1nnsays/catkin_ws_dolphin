#!/usr/bin/env python

from __future__ import print_function
import roslib
#roslib.load_manifest('../src/prosilica_driver/prosilica_camera/package.xml')
import sys
import rospy
import cv2
import numpy as np
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import sharedTypes as ST
import exitScript
import globalLandMarkInit
import maintainTracking
import searchForNewLM
import media
import csv

def showSearchMasks():
    (v,u,z) = ST.cam.frame.shape
    searchMasks = np.zeros((v,u), dtype=np.uint8)
    for LM in ST.LM_list.active:
        searchMasks = cv2.add(searchMasks, LM.mask*255)
    return searchMasks

def stateMachineRun():
    #map string to function name
    switcher = {
    "Initialize_first_LM": globalLandMarkInit.run,
    "Maintain_tracking": maintainTracking.run,
    "Search_for_new_LM": searchForNewLM.run,
    }
    func = switcher.get(ST.state) #get name of function
    success = func() #execute function, function returns 0 for fail, 1 for success
    return success


def writeToCSV(dataWriter, LMLIST): 
    # position = []
    #for LM in LMLIST:
    #    position.append(LM.loc2D[0,0])
    #    position.append(LM.loc2D[1,0])
    dataWriter.writerow(LMLIST)

def dataAppendID(csvfileName, LMLIST_ID):
    with open(csvfileName) as f:
        r = csv.reader(f)
        data = [line for line in r]
    with open(csvfileName,'w') as f:
        w = csv.writer(f)
        w.writerow(LMLIST_ID)
        w.writerows(data)





class image_converter:

    def __init__(self):

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("image_raw",Image,self.callback)

    def callback(self,data):

        try:
            
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            
            ### Preprocess Image ###
            ST.cam.frame = cv2.resize(cv_image, None, fx=media.scale,fy=media.scale,interpolation=cv2.INTER_CUBIC)
            
            ST.cam.undistortFrame()
            
            ### Detect Features #########################

            ### State Machine ###
            #clear the scan list of landmarks
            ST.LM_list.scan = []
            success = stateMachineRun() #perform the function for current state
            if success == True: #go to next state 
                if ST.state == "Initialize_first_LM":
                    ST.state = "Maintain_tracking"
                    print ("Global Init ---> Maintain Tracking ")
                elif ST.state == "Maintain_tracking":
                    ST.state = "Search_for_new_LM"
                    print ("Maintain tracking LM ---> Search for new LM")
                elif ST.state == "Search_for_new_LM":
                    ST.state = "Maintain_tracking"
                    print ("Found new LM ---> Maintain tracking ")

        except CvBridgeError as e:
            print(e)
    
        cv2.imshow("Image window", ST.cam.frame)
        cv2.imshow('Masks',showSearchMasks())


        if cv2.waitKey(3) & 0xFF == 27:
            cv2.destroyAllWindows()
            out.release()
            print ("Video out released")





def main(args):
    ### Init. Global Variables ###
    ST.init()
    
    
    
    #Grab one frame to continue init.
   # ret, ST.cam.frame = ST.cam.vidSource.read()
    ST.window = ST.Window()
    
    ### Camera Matrix ###
    ST.cam.loadCalParameters('./calibration/calibration_data.npz')
    
    ### Init. windows ###
    cv2.namedWindow('Image window') #name a window
    cv2.namedWindow('Masks')
    
    ### Data Logger ### 
    f = open("output.csv", "wb")
    writer = csv.writer(f)
    a = ['time_stamp', 'x','y']
    writer.writerows([a])

    ### Init. State Machine ###
    ST.state = "Initialize_first_LM"
    print ("Starting Global Init. ")

    ### Video Writer###
    #  fourcc = cv2.VideoWriter_fourcc(*'XVID')
    #out = cv2.VideoWriter('output.avi',fourcc, 20.0, (1360,1024))
    
    ic = image_converter()
    rospy.init_node('image_converter', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)

