#globalLandMarkInit.py
#Created 03/31/2016, kevkchoi
#Purpose: Looks for 4 corners of a black rectangle based on 4 templates

import cv2
import numpy as np
import sharedTypes as ST #this includes the global variables/objects
import exitScript
import siftDetector
import searchScript
import media

firstTimeRunning = True

def run():
    global firstTimeRunning
    if firstTimeRunning:
        firstTimeRunning = False
        matchMask = np.zeros_like(ST.cam.frame)
        listOfTemplateNames = media.imagesList
        for name in listOfTemplateNames:
            image = cv2.imread(name)
            LM = ST.LandMark(image,0,0)
            LM.matchThreshold = 0.5
            LM.sift_kp, LM.sift_des = siftDetector.siftTemplate(image)
            ST.LM_list.preload.append(LM)
        print "Loaded query images as landmarks"
        print ST.LM_list.status()
        
    scanned = searchScript.landMarkSearch("SIFT", ST.LM_list.active)
    for LM in scanned:
        ST.LM_list.scan.append(LM)
    
    if len(ST.LM_list.preload)>0:
        scanned = searchScript.landMarkSearch("SIFT", ST.LM_list.preload)
        for LM in scanned:
            ST.LM_list.preload.remove(LM)
            ST.LM_list.addToActive(LM)
            ST.LM_list.scan.append(LM)
    
    totalFound = 0
    for LM in ST.LM_list.active:
        if LM.seen > 10:
            LM.matchThreshold = 0.7
            totalFound += 1
        if LM.unseen > 5:
            LM.mask=ST.window.zone[0].copy()

    if totalFound == len(media.imagesList):
        return True
    else:
        return False

#if script is called directly from terminal, useful for debugging methods
if __name__ == "__main__":
    #firstTimeRunning = True
    ST.init()
    run()


